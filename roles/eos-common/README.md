# Ansible Role: eos-common

This role configures common items across our fleet of EOS devices that aren't specific to being a 'leaf' or a 'spine'.
Examples include but are not limited to ntp, snmp, aaa, hostnames & management interfaces.

## Requirements

yum:
python3-netaddr (can also be installed with `pip3 install netaddr`)

## Role Variables

The management interface is typically defined in the host_vars file for the switch:  
e.g.
`
management1:
  ip: 10.192.100.15/25
`

The out of band management subnet is allocated to the site-specific fabric group, such as `c99_fabric`  
e.g. `oob_net: 10.192.100.0/25`

If required, the forwarding tables can be modified to a higher L2 or L3 focus by setting `cam_partition`.  
We currently do this for the site-specific `c99_spines` group. This is a fairly hardware-specific setting.  
e.g.
`cam_partition: 4`

results table:
```
switch(config)#platform trident forwarding-table partition ?
  0  136k l2 entries, 8k l3 host, 16k lpm entries
  1  104k l2 entries, 40k l3 host, 16k lpm entries
  2  72k l2 entries, 72k l3 host, 16k lpm entries
  3  40k l2 entries, 104k l3 host, 16k lpm entries
  4  8k l2 entries, 8k l3 host, 90k lpm entries
```

### Secrets
This role sets the majority of secrets in the deployment.  
These can be held in credential stores in awx or ansible tower, but could also be available in password managers or anisible vault.  
For this role to execute, you need the following secrets available to the templates:  
e.g.
```
local_admin_password
```

## Tasks

### main.yml

This role renders all the templates in it and puts them into `/tmp/{{inventory_hostname}}/template_name.conf`

### eos_banner.yml

This task configures login and motd banners on the switch.
