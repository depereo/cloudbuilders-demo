#!/usr/bin/env python3
"""An Ansible dynamic inventory script for docker containers. This script
assumes you already have docker running on your system.

Python packages required:
- ansible>=2.1
- docker>=2.2.1

Run:
./docker-inventory.py --list
./docker-inventory.py --host my_host

Sample output:
{
    "all": {
        "hosts": ["1234"]
    },
     "_meta": {
        "hostvars": {
            "1234": {"ansible_connection": "docker"}
        }
    }
}
"""

from argparse import ArgumentParser
from copy import deepcopy
from json import dumps
import os

from docker import DockerClient

ROOT_PATH_DIR_NAME = str(os.path.basename(os.path.dirname(os.path.realpath(__file__))))


class DockerInventory(object):
    """Docker inventory class.

    This class will generate the data structure needed by ansible inventory
    host for either all containers on your system or the one you declare. The
    data structure generated is in JSON format.
    """

    _data_structure = {'all': {'hosts': []}, '_meta': {'hostvars': {}}}

    def __init__(self, option):
        """Constructor.

        When the docker inventory class is instanticated, it performs the
        following tasks:
            * Instantiate the docker client class to create a docker object.
            * Generate the JSON data structure.
            * Print the JSON data structure for ansible to use.
        """
        self.docker = DockerClient()

        if option.list:
            data = self.containers()
        elif option.host:
            data = self.containers_by_host(option.host)
        else:
            data = self._data_structure
        print(dumps(data))

    def get_containers(self):
        """Return all docker containers on the system.

        :return: Collection of containers.
        """
        return self.docker.containers.list(all=False)

    def containers(self):
        """Return all docker containers to be used by ansible inventory host.

        :return: Ansible required JSON data structure with containers.
        """
        resdata = deepcopy(self._data_structure)
        for container in self.get_containers():
            if "test-c99" in container.name:
                resdata['all']['hosts'].append(container.name)
                mgmt_network_name = f"{ROOT_PATH_DIR_NAME}_mgmt"
                if mgmt_network_name in container.__dict__['attrs']['NetworkSettings']['Networks'].keys():
                    mgmt_ip = container.__dict__['attrs']['NetworkSettings']['Networks'][mgmt_network_name]['IPAddress']
                else:
                    mgmt_ip = None
                for label in container.labels:
                    if label not in resdata.keys():
                        resdata[label] = {'hosts': []}
                    resdata[label]['hosts'].append(container.name)
                resdata['_meta']['hostvars'][container.name] = \
                        { 'ansible_connection': 'network_cli', 'ansible_network_os': 'eos', 'ansible_host': mgmt_ip, \
                                'host_key_auto_add': True, 'ansible_become': True, 'host_key_checking': False, \
                                'ansible_ssh_common_args': '-o StrictHostKeyChecking=no' }
        return resdata

    def containers_by_host(self, host=None):
        """Return the docker container requested to be used by ansible
        inventory host.

        :param host: Host name to search for.
        :return: Ansible required JSON data structure with containers.
        """
        resdata = deepcopy(self._data_structure)
        for container in self.get_containers():
            if str(container.name) == host:
                resdata['all']['hosts'].append(container.name)
                resdata['_meta']['hostvars'][container.name] = \
                    {'ansible_connection': 'docker'}
                break

        return resdata


if __name__ == "__main__":
    dynamic_parser = ArgumentParser()
    dynamic_parser.add_argument('--list', action='store_true')
    dynamic_parser.add_argument('--host')

    DockerInventory(dynamic_parser.parse_args())

